package ru.agilix.plays;

public class Play {
    private String id;
    private String name;
    private String type;

    public Play(String id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getID() {
        return id;
    }
}
